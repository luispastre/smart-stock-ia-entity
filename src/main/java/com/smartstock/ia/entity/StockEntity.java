package com.smartstock.ia.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.fasterxml.jackson.annotation.JsonBackReference;

import lombok.Setter;

@Entity
@Table(name="STOCK")
public @Setter class StockEntity {

	private Long id;
	private String symbol;
    private List<StockPredictedPriceEntity> listPredictedPrice;
    private List<StockHistoricPriceEntity> listHistoricPrice;


    @GeneratedValue(strategy = GenerationType.AUTO, generator = "native")
    @GenericGenerator(name = "native", strategy = "native")
    @Id
    @Column(name="STOCK_ID", length=15)
	public Long getId() {
		return id;
	}
    @Column(name="SYMBOL", length=120)
	public String getSymbol() {
		return symbol;
	}
    @JsonBackReference
    @OneToMany(mappedBy="stock", fetch=FetchType.LAZY, cascade = CascadeType.ALL)
	public List<StockPredictedPriceEntity> getListPredictedPrice() {
		return listPredictedPrice;
	}
    @JsonBackReference
    @OneToMany(mappedBy="stock", fetch=FetchType.LAZY, cascade = CascadeType.ALL)
	public List<StockHistoricPriceEntity> getListHistoricPrice() {
		return listHistoricPrice;
	}
    
}
