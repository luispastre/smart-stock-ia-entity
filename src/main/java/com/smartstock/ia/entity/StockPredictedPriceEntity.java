package com.smartstock.ia.entity;

import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.GenericGenerator;

import lombok.Setter;

@Entity
@Table(name="STOCK_PREDICTED_PRICE")
public @Setter class StockPredictedPriceEntity {

	private Long id;
    private Date includeDate;
	private StockEntity stock;
	private Double openPrice;
	private Double highPrice;
	private Double lowPrice;
	private Double closePrice;
	private Double volumeTmp;

	@GeneratedValue(strategy = GenerationType.AUTO, generator = "native")
    @GenericGenerator(name = "native", strategy = "native")
    @Id
    @Column(name="STOCK_PREDICTED_PRICE_ID", length=15)
	public Long getId() {
		return id;
	}

    @Column(name = "INSERT_DT", nullable = false)
    @Temporal(TemporalType.DATE)
	public Date getIncludeDate() {
		return includeDate;
	}

    @ManyToOne
    @JoinColumn(name="STOCK_ID")
	public StockEntity getStock() {
		return stock;
	}
    
    @Column(name = "OPEN_PRICE")
	public Double getOpenPrice() {
		return openPrice;
	}

    @Column(name = "HIGH_PRICE")
	public Double getHighPrice() {
		return highPrice;
	}

    @Column(name = "LOW_PRICE")
	public Double getLowPrice() {
		return lowPrice;
	}

    @Column(name = "CLOSE_PRICE")
	public Double getClosePrice() {
		return closePrice;
	}

    @Column(name = "VOLUME")
	public Double getVolumeTmp() {
		return volumeTmp;
	}
		
    @PrePersist
    public void prePersist(){
        if(this.getIncludeDate() == null){
        	Calendar brDate = Calendar.getInstance(TimeZone.getTimeZone("America/Sao_Paulo"),new Locale("pr", "BR"));
            this.setIncludeDate(brDate.getTime());
        }
    }}
